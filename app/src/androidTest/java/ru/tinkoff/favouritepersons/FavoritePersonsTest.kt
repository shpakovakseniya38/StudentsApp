package ru.tinkoff.favouritepersons

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.ok
import com.github.tomakehurst.wiremock.client.WireMock.stubFor
import com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.github.tomakehurst.wiremock.stubbing.Scenario.STARTED
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import ru.tinkoff.favouritepersons.presentation.activities.MainActivity
import ru.tinkoff.favouritepersons.room.PersonDataBase
import ru.tinkoff.favouritepersons.rules.LocalhostPreferenceRule
import ru.tinkoff.favouritepersons.screens.FavoritePersonsScreen
import ru.tinkoff.favouritepersons.utils.fileToString

class FavoritePersonsTest: TestCase() {

    private lateinit var db: PersonDataBase

    @get:Rule
    val ruleChain: RuleChain = RuleChain.outerRule(LocalhostPreferenceRule())
        .around(WireMockRule(5000))
        .around(ActivityScenarioRule(MainActivity::class.java))

    @Before
    fun createDb() {
        db = PersonDataBase.getDBClient(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    @After
    fun clearDB() {
        db.personsDao().clearTable()
    }

    // Кейс 1
    @Test
    fun hideMessageNoPersonsTest() {
        stubFor(
            get(urlEqualTo("/api/"))
                .willReturn(
                    ok(fileToString("mock/mock-add-person-1.json"))
                )
        )

        with(FavoritePersonsScreen()) {
            assertMessageNoPersonIsDisplayed()
            clickAddPersonButton()
            clickAddPersonByNetworkButton(1)
            assertMessageNoPersonIsNotDisplayed()
        }
    }

    // Кейс 2
    @Test
    fun deletePersonTest() {
        stubFor(
            get(urlEqualTo("/api/"))
                .inScenario("AddPersons")
                .whenScenarioStateIs(STARTED)
                .willSetStateTo("Step 1")
                .willReturn(
                    ok(fileToString("mock/mock-add-person-1.json"))
                )
        )
        stubFor(
            get(urlEqualTo("/api/"))
                .inScenario("AddPersons")
                .whenScenarioStateIs("Step 1")
                .willSetStateTo("Step 2")
                .willReturn(
                    ok(fileToString("mock/mock-add-person-2.json"))
                )
        )
        stubFor(
            get(urlEqualTo("/api/"))
                .inScenario("AddPersons")
                .whenScenarioStateIs("Step 2")
                .willSetStateTo("Step 3")
                .willReturn(
                    ok(fileToString("mock/mock-add-person-3.json"))
                )
        )

        with(FavoritePersonsScreen()) {
            clickAddPersonButton()
            clickAddPersonByNetworkButton(3)
            assertCountPersonItems(3)
            deletePerson(1)
            assertCountPersonItems(2)
        }
    }

    // Кейс 3
    @Test
    fun defaultSortIsCheckedTest() {
        with(FavoritePersonsScreen()) {
            clickSortButton()
            assertDefaultSortType()
        }
    }

    // Кейс 4
    @Test
    fun sortByAgeTest() {
        stubFor(
            get(urlEqualTo("/api/"))
                .inScenario("AddPersons")
                .whenScenarioStateIs(STARTED)
                .willSetStateTo("Step 1")
                .willReturn(
                    ok(fileToString("mock/mock-add-person-1.json"))
                )
        )
        stubFor(
            get(urlEqualTo("/api/"))
                .inScenario("AddPersons")
                .whenScenarioStateIs("Step 1")
                .willSetStateTo("Step 2")
                .willReturn(
                    ok(fileToString("mock/mock-add-person-2.json"))
                )
        )
        stubFor(
            get(urlEqualTo("/api/"))
                .inScenario("AddPersons")
                .whenScenarioStateIs("Step 2")
                .willSetStateTo("Step 3")
                .willReturn(
                    ok(fileToString("mock/mock-add-person-3.json"))
                )
        )

        with(FavoritePersonsScreen()) {
            clickAddPersonButton()
            clickAddPersonByNetworkButton(3)
            clickSortButton()
            assertDefaultSortType()
            clickAgeSortRadio()
            assertRadioIsClosed()
            assertPersonAge(0, "Male, 50")
            assertPersonAge(1, "Male, 40")
            assertPersonAge(2, "Female, 23")
        }
    }

    // Кейс 10
    @Test
    fun networkErrorTest() {
        device.network.disable()
        with(FavoritePersonsScreen()) {
            clickAddPersonButton()
            clickAddPersonByNetworkButton(1)
            assertNetworkErrorIsDisplayed("Internet error! Check your connection")
        }
    }

}