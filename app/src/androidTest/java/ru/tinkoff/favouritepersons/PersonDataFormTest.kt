package ru.tinkoff.favouritepersons

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import ru.tinkoff.favouritepersons.presentation.activities.MainActivity
import ru.tinkoff.favouritepersons.room.PersonDataBase
import ru.tinkoff.favouritepersons.rules.LocalhostPreferenceRule
import ru.tinkoff.favouritepersons.screens.EditPersonScreen
import ru.tinkoff.favouritepersons.screens.FavoritePersonsScreen
import ru.tinkoff.favouritepersons.utils.fileToString

class PersonDataFormTest: TestCase() {

    private lateinit var db: PersonDataBase

    @get:Rule
    val ruleChain: RuleChain = RuleChain.outerRule(LocalhostPreferenceRule())
        .around(WireMockRule(5000))
        .around(ActivityScenarioRule(MainActivity::class.java))

    @Before
    fun createDb() {
        db = PersonDataBase.getDBClient(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    @After
    fun clearDB() {
        db.personsDao().clearTable()
    }

    // Кейс 5
    @Test
    fun openEditPersonScreenTest() {
        WireMock.stubFor(
            WireMock.get(WireMock.urlEqualTo("/api/"))
                .willReturn(
                    WireMock.ok(fileToString("mock/mock-add-person-3.json"))
                )
        )

        with(FavoritePersonsScreen()) {
            clickAddPersonButton()
            clickAddPersonByNetworkButton(1)
            openEditPersonScreen(0)
        }

        with(EditPersonScreen()) {
            assertScreenTitle("Редактирование пользователя")
            assertPersonName("Owen")
            assertPersonSurname("Guerin")
            assertPersonGender("М")
            assertPersonBirthDate("1973-11-13")
        }
    }

    // Кейс 6
    @Test
    fun editPersonTest() {
        WireMock.stubFor(
            WireMock.get(WireMock.urlEqualTo("/api/"))
                .willReturn(
                    WireMock.ok(fileToString("mock/mock-add-person-3.json"))
                )
        )

        with(FavoritePersonsScreen()) {
            clickAddPersonButton()
            clickAddPersonByNetworkButton(1)
            openEditPersonScreen(0)
        }

        with(EditPersonScreen()) {
            assertScreenTitle("Редактирование пользователя")
            typeTextToNameField("Иосиф")
            clickSubmitButton()
        }

        FavoritePersonsScreen().assertPersonName(0, "Иосиф Guerin")
    }

    // Кейс 7
    @Test
    fun addPersonManuallyTest() {
        with(FavoritePersonsScreen()) {
            clickAddPersonButton()
            clickAddPersonManuallyButton()
        }

        with(EditPersonScreen()) {
            assertScreenTitle("Добавление пользователя")
            fillDefaultPersonForm()
            clickSubmitButton()
        }

        with( FavoritePersonsScreen()) {
            assertPersonName(0, "Иван Иванов")
            assertPersonAge(0, "Male, 70")
        }
    }

    // Кейс 8
    @Test
    fun errorEmptyGenderTest() {
        with(FavoritePersonsScreen()) {
            clickAddPersonButton()
            clickAddPersonManuallyButton()
        }

        with(EditPersonScreen()) {
            assertScreenTitle("Добавление пользователя")
            clickSubmitButton()
            assertErrorEmptyGenderMessageIsDisplayed()
        }
    }

    // Кейс 9
    @Test
    fun hideErrorEmptyGenderTest() {
        with(FavoritePersonsScreen()) {
            clickAddPersonButton()
            clickAddPersonManuallyButton()
        }

        with(EditPersonScreen()) {
            assertScreenTitle("Добавление пользователя")
            typeTextToNameField("Иван")
            typeTextToSurnameField("Иванов")
            typeTextToBirthDateField("1953-12-13")
            typeTextToEmailField("ivan@ivan.ru")
            typeTextToPhoneField("+79041234567")
            typeTextToAddressField("Улица Пушкина дом 10")
            typeTextToPhotoLinkField("https://randomuser.me/api/portraits/med/men/72.jpg")
            typeTextToScoreField("99")
            clickSubmitButton()
            assertErrorEmptyGenderMessageIsDisplayed()
            typeTextToGenderField("М")
            assertErrorEmptyGenderMessageIsNotDisplayed()
        }
    }
}