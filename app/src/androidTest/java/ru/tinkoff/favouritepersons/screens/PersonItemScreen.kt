package ru.tinkoff.favouritepersons.screens

import android.view.View
import io.github.kakaocup.kakao.recycler.KRecyclerItem
import io.github.kakaocup.kakao.text.KTextView
import org.hamcrest.Matcher
import ru.tinkoff.favouritepersons.R
//import androidx.test.platform.app.InstrumentationRegistry

class PersonItemScreen(matcher: Matcher<View>): KRecyclerItem<PersonItemScreen>(matcher) {
    val personPrivateInfo = KTextView(matcher) { withId(R.id.person_private_info) }
    val personName = KTextView(matcher) { withId(R.id.person_name) }
    //val nameText = InstrumentationRegistry.getInstrumentation().targetContext.getString(R.id.person_name)
}