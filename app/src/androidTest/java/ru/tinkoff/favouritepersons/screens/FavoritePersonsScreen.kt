package ru.tinkoff.favouritepersons.screens

import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isChecked
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.recycler.KRecyclerView
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import org.junit.Assert.assertEquals
import ru.tinkoff.favouritepersons.R

class FavoritePersonsScreen: BaseScreen() {
    private val messageNoPersons = KTextView { withId(R.id.tw_no_persons) }

    private val addPersonButton = KButton { withId(R.id.fab_add_person) }
    private val addPersonManuallyButton = KButton { withId(R.id.fab_add_person_manually) }
    private val addPersonByNetworkButton = KButton { withId(R.id.fab_add_person_by_network) }

    private val sortButton = KButton { withId(R.id.action_item_sort) }
    private val defaultSortRadio = KButton { withId(R.id.bsd_rb_default) }
    private val ageSortRadio = KButton { withId(R.id.bsd_rb_age) }

    private val networkErrorMessage = KTextView { withId(com.google.android.material.R.id.snackbar_text) }
    private val bottomSheet = KView { withId(R.id.bottom_sheet) }
    private val personItems = KRecyclerView (
        builder = { withId(R.id.rv_person_list) },
        itemTypeBuilder = {itemType (::PersonItemScreen) }
    )

    // Actions
    fun clickAddPersonButton() {
        addPersonButton.click()
    }

    fun clickAddPersonManuallyButton() {
        addPersonManuallyButton.click()
    }

    fun clickAddPersonByNetworkButton(count: Int) {
        for (i in 1 .. count) {
            addPersonByNetworkButton.click()
        }
    }

    fun clickSortButton() {
        sortButton.click()
    }

    fun clickAgeSortRadio() {
        ageSortRadio.isDisplayed()
        ageSortRadio.isClickable()
        ageSortRadio.click()
    }

    fun openEditPersonScreen(position: Int) {
        personItems.childAt<PersonItemScreen>(position) {
            view.perform(ViewActions.click())
        }
    }
    fun deletePerson(position: Int) {
        personItems.childAt<PersonItemScreen>(position) {
            view.perform(ViewActions.swipeLeft())
        }
    }

    // Assertions
    fun assertMessageNoPersonIsDisplayed() {
        messageNoPersons.isDisplayed()
    }

    fun assertMessageNoPersonIsNotDisplayed() {
        messageNoPersons.isNotDisplayed()
    }

    fun assertCountPersonItems(count: Int) {
        assertEquals(count, personItems.getSize())
    }

    fun assertDefaultSortType() {
        defaultSortRadio.assert { matches(isChecked()) }
    }

    fun assertAgeSortIsChecked() {
        ageSortRadio.assert { matches(isChecked()) }
    }

    fun assertRadioIsClosed() {
        bottomSheet.doesNotExist()
    }

    fun assertPersonAge(position: Int, age: String) {
        personItems.childAt<PersonItemScreen>(position) {
            personPrivateInfo.hasText(age)
        }
    }

    fun assertPersonName(position: Int, name: String) {
        personItems.childAt<PersonItemScreen>(position) {
            personName.hasText(name)
        }
    }

    fun assertNetworkErrorIsDisplayed(text: String) {
        networkErrorMessage.isDisplayed()
        networkErrorMessage.hasText(text)
    }
}