package ru.tinkoff.favouritepersons.screens

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import org.hamcrest.Matchers.allOf
import ru.tinkoff.favouritepersons.R

class EditPersonScreen: BaseScreen() {
    private val screenTitle = KTextView { withId(R.id.tw_person_screen_title) }

    private val birthDateField = KEditText { withId(R.id.et_birthdate) }
    private val nameField = KEditText { withId(R.id.et_name) }
    private val surnameField = KEditText { withId(R.id.et_surname) }
    private val genderField = KEditText { withId(R.id.et_gender) }
    private val emailField = KEditText { withId(R.id.et_email) }
    private val phoneField = KEditText { withId(R.id.et_phone) }
    private val addressField = KEditText { withId(R.id.et_address) }
    private val photoLinkField = KEditText { withId(R.id.et_image) }
    private val scoreField = KEditText { withId(R.id.et_score) }
    private val errorMessage = onView( allOf(
        withId(com.google.android.material.R.id.textinput_error),
        withText("Поле должно быть заполнено буквами М или Ж")))

    private val submitButton = KButton { withId(R.id.submit_button) }

    // Actions
    fun typeTextToNameField(text: String) {
        nameField.clearText()
        nameField.replaceText(text)
    }

    fun typeTextToSurnameField(text: String) {
        surnameField.clearText()
        surnameField.replaceText(text)
    }

    fun typeTextToGenderField(text: String){
        genderField.clearText()
        genderField.replaceText(text)
    }

    fun typeTextToBirthDateField(text: String){
        birthDateField.clearText()
        birthDateField.replaceText(text)
    }

    fun typeTextToEmailField(text: String){
        emailField.clearText()
        emailField.replaceText(text)
    }

    fun typeTextToPhoneField(text: String){
        phoneField.clearText()
        phoneField.replaceText(text)
    }

    fun typeTextToAddressField(text: String){
        addressField.clearText()
        addressField.replaceText(text)
    }

    fun typeTextToPhotoLinkField(text: String){
        photoLinkField.clearText()
        photoLinkField.replaceText(text)
    }

    fun typeTextToScoreField(text: String){
        scoreField.clearText()
        scoreField.replaceText(text)
    }

    fun fillDefaultPersonForm() {
        typeTextToNameField("Иван")
        typeTextToSurnameField("Иванов")
        typeTextToGenderField("М")
        typeTextToBirthDateField("1953-12-13")
        typeTextToEmailField("ivan@ivan.ru")
        typeTextToPhoneField("+79041234567")
        typeTextToAddressField("Улица Пушкина дом 10")
        typeTextToPhotoLinkField("https://randomuser.me/api/portraits/med/men/72.jpg")
        typeTextToScoreField("99")
    }

    fun clickSubmitButton() {
        submitButton.click()
    }

    // Assertions
    fun assertScreenTitle(text: String) {
        screenTitle.hasText(text)
    }

    fun assertPersonName(name: String) {
        nameField.hasText(name)
    }

    fun assertPersonSurname(surname: String) {
        surnameField.hasText(surname)
    }

    fun assertPersonGender(text: String) {
        genderField.hasText(text)
    }

    fun assertPersonBirthDate(text: String) {
        birthDateField.hasText(text)
    }

    fun assertErrorEmptyGenderMessageIsDisplayed() {
        errorMessage.check( matches(isDisplayed()) )
    }

    fun assertErrorEmptyGenderMessageIsNotDisplayed() {
        errorMessage.check(doesNotExist())
    }

}